package ic.gui;

import java.awt.Container;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import static javax.swing.SpringLayout.NORTH;
import static javax.swing.SpringLayout.SOUTH;
import static javax.swing.SpringLayout.WEST;
import static javax.swing.SpringLayout.EAST;

@SuppressWarnings("serial")
public class Frame extends JFrame {
	
	private static Frame INSTANCE;
	private InputPanel inputPanel;
	private GraphPanel graphPanel;

	public Frame() {
		super("Integral Calc");
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Container contentPane = getContentPane();
		SpringLayout layout = new SpringLayout();
		contentPane.setLayout(layout);
		
		inputPanel = new InputPanel();
		layout.putConstraint(NORTH, inputPanel, 5, NORTH, contentPane);
		layout.putConstraint(SOUTH, inputPanel, 100, NORTH, inputPanel);
		layout.putConstraint(WEST, inputPanel, 5, WEST, contentPane);
		layout.putConstraint(EAST, inputPanel, -5, EAST, contentPane);
		add(inputPanel);
		
		graphPanel = new GraphPanel();
		layout.putConstraint(NORTH, graphPanel, 5, SOUTH, inputPanel);
		layout.putConstraint(SOUTH, graphPanel, -5, SOUTH, contentPane);
		layout.putConstraint(WEST, graphPanel, 5, WEST, contentPane);
		layout.putConstraint(EAST, graphPanel, -5, EAST, contentPane);
		add(graphPanel);
		
		contentPane.setPreferredSize(new Dimension(1200, 800));
		setLocation(200, 100);
		pack();
		setVisible(true);
	}
	
	
	public GraphPanel getGraphPanel() {
		return graphPanel;
	}
	
	public InputPanel getInputPanel() {
		return inputPanel;
	}
	
	public static void initFrame() {
		SwingUtilities.invokeLater(() -> {
			INSTANCE = new Frame();
		});
	}
	
	public static Frame getFrame() {
		return INSTANCE;
	}
	
}
