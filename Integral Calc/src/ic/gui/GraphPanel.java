package ic.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.AffineTransform;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

import com.fathzer.soft.javaluator.DoubleEvaluator;
import com.fathzer.soft.javaluator.StaticVariableSet;

import ic.util.Function;
import ic.util.IntegralMath;
import ic.util.Interval;
import ic.util.Polygon;

import static javax.swing.SpringLayout.NORTH;
import static javax.swing.SpringLayout.SOUTH;
import static javax.swing.SpringLayout.WEST;
import static javax.swing.SpringLayout.EAST;

@SuppressWarnings("serial")
public class GraphPanel extends JPanel {

	private DrawPanel drawPanel;
	private OutputPanel outputPanel;
	
	public GraphPanel() {

		setBorder(BorderFactory.createTitledBorder("Graph"));
		setLayout(new SpringLayout());

		createAndAddComponents();
	}

	private void createAndAddComponents() {
		SpringLayout layout = (SpringLayout) getLayout();

		JLabel xMinLabel = new JLabel("x min:", SwingConstants.CENTER);
		layout.putConstraint(NORTH, xMinLabel, 5, NORTH, this);
		layout.putConstraint(SOUTH, xMinLabel, 20, NORTH, xMinLabel);		
		layout.putConstraint(WEST, xMinLabel, 5, WEST, this);
		add(xMinLabel);

		JTextField xMinField = new JTextField("-20");
		layout.putConstraint(NORTH, xMinField, 5, NORTH, this);
		layout.putConstraint(SOUTH, xMinField, 20, NORTH, xMinField);
		layout.putConstraint(WEST, xMinField, 5, EAST, xMinLabel);
		layout.putConstraint(EAST, xMinField, 40, WEST, xMinField);
		add(xMinField);

		JLabel xMaxLabel = new JLabel("x max:", SwingConstants.CENTER);
		layout.putConstraint(NORTH, xMaxLabel, 5, NORTH, this);
		layout.putConstraint(SOUTH, xMaxLabel, 20, NORTH, xMaxLabel);		
		layout.putConstraint(WEST, xMaxLabel, 5, EAST, xMinField);
		add(xMaxLabel);

		JTextField xMaxField = new JTextField("20");
		layout.putConstraint(NORTH, xMaxField, 5, NORTH, this);
		layout.putConstraint(SOUTH, xMaxField, 20, NORTH, xMaxField);
		layout.putConstraint(WEST, xMaxField, 5, EAST, xMaxLabel);
		layout.putConstraint(EAST, xMaxField, 40, WEST, xMaxField);
		add(xMaxField);

		JLabel yMinLabel = new JLabel("y min:", SwingConstants.CENTER);
		layout.putConstraint(NORTH, yMinLabel, 5, SOUTH, xMinLabel);
		layout.putConstraint(SOUTH, yMinLabel, 20, NORTH, yMinLabel);		
		layout.putConstraint(WEST, yMinLabel, 5, WEST, this);
		add(yMinLabel);

		JTextField yMinField = new JTextField("-20");
		layout.putConstraint(NORTH, yMinField, 5, SOUTH, xMinField);
		layout.putConstraint(SOUTH, yMinField, 20, NORTH, yMinField);
		layout.putConstraint(WEST, yMinField, 5, EAST, yMinLabel);
		layout.putConstraint(EAST, yMinField, 40, WEST, yMinField);
		add(yMinField);

		JLabel yMaxLabel = new JLabel("y max:", SwingConstants.CENTER);
		layout.putConstraint(NORTH, yMaxLabel, 5, SOUTH, xMaxLabel);
		layout.putConstraint(SOUTH, yMaxLabel, 20, NORTH, yMaxLabel);		
		layout.putConstraint(WEST, yMaxLabel, 5, EAST, yMinField);
		add(yMaxLabel);

		JTextField yMaxField = new JTextField("20");
		layout.putConstraint(NORTH, yMaxField, 5, SOUTH, xMaxField);
		layout.putConstraint(SOUTH, yMaxField, 20, NORTH, yMaxField);
		layout.putConstraint(WEST, yMaxField, 5, EAST, yMaxLabel);
		layout.putConstraint(EAST, yMaxField, 40, WEST, yMaxField);
		add(yMaxField);

		GridControlPanel gridPanel = new GridControlPanel();
		layout.putConstraint(NORTH, gridPanel, 10, SOUTH, yMinLabel);
		layout.putConstraint(SOUTH, gridPanel, 75, NORTH, gridPanel);
		layout.putConstraint(WEST, gridPanel, 0, WEST, this);
		layout.putConstraint(EAST, gridPanel, 0, EAST, xMaxField);
		add(gridPanel);

		outputPanel = new OutputPanel();
		layout.putConstraint(NORTH, outputPanel, 10, SOUTH, gridPanel);
		layout.putConstraint(SOUTH, outputPanel, -5, SOUTH, this);
		layout.putConstraint(WEST, outputPanel, 0, WEST, this);
		layout.putConstraint(EAST, outputPanel, 0, EAST, xMaxField);
		add(outputPanel);

		drawPanel = new DrawPanel();
		layout.putConstraint(NORTH, drawPanel, 5, NORTH, this);
		layout.putConstraint(SOUTH, drawPanel, -5, SOUTH, this);
		layout.putConstraint(WEST, drawPanel, 5, EAST, xMaxField);
		layout.putConstraint(EAST, drawPanel, -5, EAST, this);
		add(drawPanel);

		ActionListener graphSpaceUpdateListener = e -> {

			try {

				DoubleEvaluator eval = new DoubleEvaluator();

				double xMin, xMax, yMin, yMax;

				xMin = eval.evaluate(xMinField.getText());
				xMax = eval.evaluate(xMaxField.getText());
				yMin = eval.evaluate(yMinField.getText());
				yMax = eval.evaluate(yMaxField.getText());

				drawPanel.setViewPort(xMin, xMax, yMin, yMax);

			}catch (IllegalArgumentException e1) {
				JOptionPane.showMessageDialog(this, "Couldn't evaluate all fields content to a number.", "Resize failed", JOptionPane.ERROR_MESSAGE);
			}

		};

		xMinField.addActionListener(graphSpaceUpdateListener);
		xMaxField.addActionListener(graphSpaceUpdateListener);
		yMinField.addActionListener(graphSpaceUpdateListener);
		yMaxField.addActionListener(graphSpaceUpdateListener);

	}

	public void redraw() {
		drawPanel.repaint();
	}

	public void setGridSpacing(double horizontal, double vertical) {
		drawPanel.setHorizontalGridSpacing(horizontal);
		drawPanel.setVerticalGridSpacing(vertical);
		redraw();
	}

	public void setResult(double result) {
		outputPanel.setArea(result);
	}

}

@SuppressWarnings("serial")
class DrawPanel extends JPanel {	

	private static final Font GRAPH_FONT = new Font("Consolas", Font.PLAIN, 10);
	
	private static final Color GRID_COLOR = new Color(200, 200, 200);
	private static final Color AXES_NUMBER_COLOR = new Color(0, 0, 0);
	private static final Color AXES_COLOR = new Color(0, 0, 0);
	
	private AffineTransform flipMatrix;

	private Interval graphSpaceX, graphSpaceY;
	private Interval pxSpaceX, pxSpaceY;

	private double horizontalGridSpacing = 2;
	private double verticalGridSpacing = 2;

	public DrawPanel() {
		setBackground(new Color(255, 255, 255));
		
		pxSpaceX = new Interval(0, getWidth());
		pxSpaceY = new Interval(0, getHeight());

		graphSpaceX = new Interval(-20, 20);
		graphSpaceY = new Interval(-20, 20);

		flipMatrix = new AffineTransform(1, 0, 0, -1, 0, getHeight());

		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				pxSpaceX = new Interval(0, getWidth());
				pxSpaceY = new Interval(0, getHeight());

				flipMatrix = new AffineTransform(1, 0, 0, -1, 0, getHeight());
			}
		});

	}

	public void setViewPort(double xMin, double xMax, double yMin, double yMax) {

		graphSpaceX = new Interval(xMin, xMax);
		graphSpaceY = new Interval(yMin, yMax);

		repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.setFont(GRAPH_FONT);
		FontMetrics metrics = g.getFontMetrics(GRAPH_FONT);
		
		Graphics2D g2d = (Graphics2D) g;
		
		g2d.transform(flipMatrix);
		
		for(double d = Math.ceil(graphSpaceX.a/horizontalGridSpacing)*horizontalGridSpacing; d < graphSpaceX.b; d += horizontalGridSpacing) {
			
			g.setColor(GRID_COLOR);
			
			g.drawLine(
					(int)toPxSpaceX(d),
					(int)toPxSpaceY(graphSpaceY.a),
					(int)toPxSpaceX(d),
					(int)toPxSpaceY(graphSpaceY.b)
					);
			
			if(d == 0) {
				continue;
			}
			
			g.setColor(AXES_NUMBER_COLOR);
			drawStringCenteredUnder(g2d, (int)toPxSpaceX(d), (int)toPxSpaceY(0), Double.toString(d), metrics);
			
		}

		for(double d = Math.ceil(graphSpaceY.a/verticalGridSpacing)*verticalGridSpacing; d < graphSpaceY.b; d += verticalGridSpacing) {
			
			g.setColor(GRID_COLOR);
			
			g.drawLine(
					(int)toPxSpaceX(graphSpaceX.a),
					(int)toPxSpaceY(d),
					(int)toPxSpaceX(graphSpaceX.b),
					(int)toPxSpaceY(d)
					);
			
			if(d == 0) {
				continue;
			}
			
			g.setColor(AXES_NUMBER_COLOR);
			drawStringCenteredLeftTo(g2d, (int)toPxSpaceX(0), (int)toPxSpaceY(d), Double.toString(d), metrics);
		}
		
		drawStringUpAndRightTo(g2d, (int)toPxSpaceX(0), (int)toPxSpaceY(0), Double.toString(0), metrics);
		
		g.setColor(AXES_COLOR);
		
		g.drawLine(
				(int)toPxSpaceX(graphSpaceX.a),
				(int)toPxSpaceY(0),
				(int)toPxSpaceX(graphSpaceX.b),
				(int)toPxSpaceY(0)
				);

		g.drawLine(
				(int)toPxSpaceX(0),
				(int)toPxSpaceY(graphSpaceY.a),
				(int)toPxSpaceX(0),
				(int)toPxSpaceY(graphSpaceY.b)
				);

		if(IntegralMath.currentPolygon != null) {
			drawPolygon(IntegralMath.currentPolygon, (Graphics2D)g);
		}

		for(int i = 0; i < IntegralMath.FUNCTIONS.size(); i++) {	
			drawFunction(IntegralMath.FUNCTIONS.get(i), g);
		}

	}
	
	private void drawStringCenteredUnder(Graphics2D g2d, int xPx, int yPx, String text, FontMetrics metrics) {
		
		xPx -= metrics.stringWidth(text)/2;
		yPx -= metrics.getHeight() - metrics.getAscent() + 10;
		
		g2d.transform(flipMatrix);
		g2d.drawString(text, xPx, getHeight() - yPx);
		g2d.transform(flipMatrix);
	}
	
	private void drawStringCenteredLeftTo(Graphics2D g2d, int xPx, int yPx, String text, FontMetrics metrics) {
		
		xPx -= metrics.stringWidth(text) + 5;
		yPx -= metrics.getHeight() - metrics.getAscent();
		
		g2d.transform(flipMatrix);
		g2d.drawString(text, xPx, getHeight() - yPx);
		g2d.transform(flipMatrix);
		
	}
	
	private void drawStringUpAndRightTo(Graphics2D g2d, int xPx, int yPx, String text, FontMetrics metrics) {
		
		xPx -= metrics.stringWidth(text) + 5;
		yPx -= metrics.getHeight() - metrics.getAscent() + 10;
		
		g2d.transform(flipMatrix);
		g2d.drawString(text, xPx, getHeight() - yPx);
		g2d.transform(flipMatrix);
	}
	
	private void drawFunction(Function f, Graphics g) {
		g.setColor(f.getColor());

		DoubleEvaluator eval = new DoubleEvaluator();
		final StaticVariableSet<Double> vars = new StaticVariableSet<>();

		double x0 = toGraphSpaceX(0);
		vars.set("x", x0);
		double y0;

		try {
			y0 = eval.evaluate(f.getExpression(), vars);
		}catch (IllegalArgumentException e) {
			y0 = Double.NaN;
		}

		double x1, y1;

		for(double d = 1; d < getWidth(); d += 0.1) {

			x1 = toGraphSpaceX(d);
			vars.set("x", x1);

			try {
				y1 = eval.evaluate(f.getExpression(), vars);
			}catch (IllegalArgumentException e) {
				y1 = Double.NaN;
			}

			if(Double.isFinite(y0) && Double.isFinite(y1)) {

				g.drawLine((int)toPxSpaceX(x0),
						(int)toPxSpaceY(y0),
						(int)toPxSpaceX(x1),
						(int)toPxSpaceY(y1)
						);
			}

			x0 = x1;
			y0 = y1;
		}

	}

	private void drawPolygon(Polygon p, Graphics2D g) {

		int[] xPxPositions = new int[p.getVertexCount()];
		int[] yPxPositions = new int[p.getVertexCount()];

		for(int i = 0; i < p.getVertexCount(); i++) {
			
			try {
				xPxPositions[i] = (int) toPxSpaceX(p.getVertices()[i].x);
				yPxPositions[i] = (int) toPxSpaceY(p.getVertices()[i].y);
			}catch (Exception e){
				e.printStackTrace();
				System.err.println(i);
			}
			
		}

		java.awt.Polygon awtPoly = new java.awt.Polygon(xPxPositions, yPxPositions, p.getVertexCount());

		g.setColor(new Color(
				p.getColor().getRed(),
				p.getColor().getGreen(),
				p.getColor().getBlue(),
				80));
		g.fill(awtPoly);


		g.setColor(p.getColor());
		g.draw(awtPoly);
		
	}

	private double toPxSpaceX(double graphX) {
		return graphSpaceX.map(pxSpaceX, graphX);
	}

	private double toPxSpaceY(double graphY) {
		return graphSpaceY.map(pxSpaceY, graphY);
	}

	private double toGraphSpaceX(double pxX) {
		return pxSpaceX.map(graphSpaceX, pxX);
	}

	public void setHorizontalGridSpacing(double gridSpacing) {
		this.horizontalGridSpacing = gridSpacing;
	}

	public void setVerticalGridSpacing(double gridSpacing) {
		this.verticalGridSpacing = gridSpacing;
	}

}

@SuppressWarnings("serial")
class GridControlPanel extends JPanel {

	public GridControlPanel() {

		setBorder(BorderFactory.createTitledBorder("Grid"));
		SpringLayout layout = new SpringLayout();
		setLayout(layout);

		JLabel horizontalSpacingLabel = new JLabel("Horizontal spacing:", SwingConstants.CENTER);
		layout.putConstraint(NORTH, horizontalSpacingLabel, 5, NORTH, this);
		layout.putConstraint(SOUTH, horizontalSpacingLabel, 20, NORTH, horizontalSpacingLabel);
		layout.putConstraint(WEST, horizontalSpacingLabel, 5, WEST, this);
		add(horizontalSpacingLabel);

		JTextField horizontalSpacingField = new JTextField("2");
		layout.putConstraint(NORTH, horizontalSpacingField, 5, NORTH, this);
		layout.putConstraint(SOUTH, horizontalSpacingField, 20, NORTH, horizontalSpacingField);
		layout.putConstraint(WEST, horizontalSpacingField, -40, EAST, horizontalSpacingField);
		layout.putConstraint(EAST, horizontalSpacingField, -5, EAST, this);
		add(horizontalSpacingField);

		JLabel verticalSpacingLabel = new JLabel("Vertical spacing:", SwingConstants.CENTER);
		layout.putConstraint(NORTH, verticalSpacingLabel, 5, SOUTH, horizontalSpacingLabel);
		layout.putConstraint(SOUTH, verticalSpacingLabel, 20, NORTH, verticalSpacingLabel);
		layout.putConstraint(WEST, verticalSpacingLabel, 5, WEST, this);
		add(verticalSpacingLabel);

		JTextField verticalSpacingField = new JTextField("2");
		layout.putConstraint(NORTH, verticalSpacingField, 5, SOUTH, horizontalSpacingField);
		layout.putConstraint(SOUTH, verticalSpacingField, 20, NORTH, verticalSpacingField);
		layout.putConstraint(WEST, verticalSpacingField, -40, EAST, verticalSpacingField);
		layout.putConstraint(EAST, verticalSpacingField, -5, EAST, this);
		add(verticalSpacingField);

		ActionListener respaceListener = e -> {
			DoubleEvaluator eval = new DoubleEvaluator();

			Frame.getFrame().getGraphPanel().setGridSpacing(eval.evaluate(horizontalSpacingField.getText()), eval.evaluate(verticalSpacingField.getText()));
		};

		horizontalSpacingField.addActionListener(respaceListener);
		verticalSpacingField.addActionListener(respaceListener);

	}

}

@SuppressWarnings("serial")
class OutputPanel extends JPanel {

	private JTextField areaField;
	
	public OutputPanel() {

		setBorder(BorderFactory.createTitledBorder("Output"));
		SpringLayout layout = new SpringLayout();
		setLayout(layout);

		JLabel areaLabel = new JLabel("Area:", SwingConstants.CENTER);
		layout.putConstraint(NORTH, areaLabel, 5, NORTH, this);
		layout.putConstraint(SOUTH, areaLabel, 20, NORTH, areaLabel);
		layout.putConstraint(WEST, areaLabel, 5, WEST, this);
		add(areaLabel);
		
		areaField = new JTextField("");
		layout.putConstraint(NORTH, areaField, 5, SOUTH, areaLabel);
		layout.putConstraint(SOUTH, areaField, 20, NORTH, areaField);
		layout.putConstraint(WEST, areaField, 5, WEST, this);
		layout.putConstraint(EAST, areaField, -5, EAST, this);
		add(areaField);
		areaField.setEditable(false);
		
	}
	
	public void setArea(double area) {
		areaField.setText(Double.toString(area));
	}

}
