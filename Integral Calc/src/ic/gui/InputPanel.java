package ic.gui;

import static javax.swing.SpringLayout.EAST;
import static javax.swing.SpringLayout.NORTH;
import static javax.swing.SpringLayout.SOUTH;
import static javax.swing.SpringLayout.WEST;

import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import ic.util.Function;
import ic.util.IntegralMath;
import ic.util.IntegrationMethod;
import ic.util.Polygon;

@SuppressWarnings("serial")
public class InputPanel extends JPanel {
	
	private JComboBox<Function> functionSelection;
	
	public InputPanel() {
		
		setBorder(BorderFactory.createTitledBorder("Input"));
		SpringLayout layout = new SpringLayout();
		setLayout(layout);
		
		JTextField inputField = new JTextField();
		layout.putConstraint(NORTH, inputField, 5, NORTH, this);
		layout.putConstraint(SOUTH, inputField, 20, NORTH, inputField);
		layout.putConstraint(WEST, inputField, 5, WEST, this);
		layout.putConstraint(EAST, inputField, -5, EAST, this);
		add(inputField);
		
		inputField.addActionListener(e -> {
			if(inputField.getText().matches(Function.REGEX)) {
				Function.addFunction(inputField.getText());
				inputField.setText("");
			}
			
			
		});
		
		JButton integrateButton = new JButton("Integrate");
		layout.putConstraint(NORTH, integrateButton, -20, SOUTH, integrateButton);
		layout.putConstraint(SOUTH, integrateButton, -10, SOUTH, this);
		layout.putConstraint(WEST, integrateButton, 5, WEST, this);
		layout.putConstraint(EAST, integrateButton, 100, WEST, integrateButton);
		add(integrateButton);
		
		IntervalPanel intervalPanel = new IntervalPanel("0", "1");
		layout.putConstraint(NORTH, intervalPanel, -20, SOUTH, intervalPanel);
		layout.putConstraint(SOUTH, intervalPanel, -10, SOUTH, this);
		layout.putConstraint(WEST, intervalPanel, 50, EAST, integrateButton);
		add(intervalPanel);
		
		functionSelection = new JComboBox<>();
		layout.putConstraint(NORTH, functionSelection, -20, SOUTH, functionSelection);
		layout.putConstraint(SOUTH, functionSelection, -10, SOUTH, this);
		layout.putConstraint(WEST, functionSelection, 50, EAST, intervalPanel);
		layout.putConstraint(EAST, functionSelection, 150, WEST, functionSelection);
		add(functionSelection);
		
		updateFunctionModel();
		
		JLabel subIntervalLabel = new JLabel("Subintervals:");
		layout.putConstraint(NORTH, subIntervalLabel, -20, SOUTH, subIntervalLabel);
		layout.putConstraint(SOUTH, subIntervalLabel, -10, SOUTH, this);
		layout.putConstraint(WEST, subIntervalLabel, 50, EAST, functionSelection);
		add(subIntervalLabel);
		
		JTextField subIntervalField = new JTextField();
		layout.putConstraint(NORTH, subIntervalField, -20, SOUTH, subIntervalField);
		layout.putConstraint(SOUTH, subIntervalField, -10, SOUTH, this);
		layout.putConstraint(WEST, subIntervalField, 0, EAST, subIntervalLabel);
		layout.putConstraint(EAST, subIntervalField, 40, WEST, subIntervalField);
		add(subIntervalField);
		
		JComboBox<IntegrationMethod> methodSelection = new JComboBox<>(IntegrationMethod.LIST);
		layout.putConstraint(NORTH, methodSelection, -20, SOUTH, methodSelection);
		layout.putConstraint(SOUTH, methodSelection, -10, SOUTH, this);
		layout.putConstraint(WEST, methodSelection, 50, EAST, subIntervalField);
		layout.putConstraint(EAST, methodSelection, 150, WEST, methodSelection);
		add(methodSelection);
		
		ActionListener integrateListener = e -> {
			
			Polygon.drawPolygon((Function)functionSelection.getSelectedItem(), intervalPanel.getInterval(), Integer.parseInt(subIntervalField.getText()), (IntegrationMethod)methodSelection.getSelectedItem());
			
			double result = ((IntegrationMethod)methodSelection.getSelectedItem()).integral((Function)functionSelection.getSelectedItem(), intervalPanel.getInterval(), Integer.parseInt(subIntervalField.getText()));
			
			Frame.getFrame().getGraphPanel().setResult(result);
			
			
		};
		
		integrateButton.addActionListener(integrateListener);
		
	}

	public void updateFunctionModel() {
		functionSelection.setModel(new DefaultComboBoxModel<>(IntegralMath.FUNCTIONS));
	}

}
