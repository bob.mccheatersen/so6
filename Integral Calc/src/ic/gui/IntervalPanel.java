package ic.gui;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import com.fathzer.soft.javaluator.DoubleEvaluator;

import ic.util.Interval;

import static javax.swing.SpringLayout.NORTH;
import static javax.swing.SpringLayout.SOUTH;
import static javax.swing.SpringLayout.WEST;
import static javax.swing.SpringLayout.EAST;

@SuppressWarnings("serial")
public class IntervalPanel extends JPanel {
	
	private JTextField aField, bField;
	
	
	public IntervalPanel(String a, String b) {
		
		SpringLayout layout = new SpringLayout();
		setLayout(layout);
		
		JLabel label1 = new JLabel("Interval: [", SwingUtilities.CENTER);
		layout.putConstraint(NORTH, label1, 0, NORTH, this);
		layout.putConstraint(SOUTH, label1, 0, SOUTH, this);
		layout.putConstraint(WEST, label1, 0, WEST, this);
		add(label1);
		
		aField = new JTextField(a);
		layout.putConstraint(NORTH, aField, 0, NORTH, this);
		layout.putConstraint(SOUTH, aField, 0, SOUTH, this);
		layout.putConstraint(WEST, aField, 1, EAST, label1);
		layout.putConstraint(EAST, aField, 36, WEST, aField);
		add(aField);
		
		JLabel label2 = new JLabel(",", SwingConstants.CENTER);
		layout.putConstraint(NORTH, label2, 0, NORTH, this);
		layout.putConstraint(SOUTH, label2, 0, SOUTH, this);
		layout.putConstraint(WEST, label2, 1, EAST, aField);
		add(label2);
		
		bField = new JTextField(b);
		layout.putConstraint(NORTH, bField, 0, NORTH, this);
		layout.putConstraint(SOUTH, bField, 0, SOUTH, this);
		layout.putConstraint(WEST, bField, 1, EAST, label2);
		layout.putConstraint(EAST, bField, 36, WEST, bField);
		add(bField);
		
		JLabel label3 = new JLabel("]", SwingConstants.CENTER);
		layout.putConstraint(NORTH, label3, 0, NORTH, this);
		layout.putConstraint(SOUTH, label3, 0, SOUTH, this);
		layout.putConstraint(WEST, label3, 1, EAST, bField);
		add(label3);
		
		setPreferredSize(new Dimension(131, 20));
	}
	
	public Interval getInterval() {
		
		DoubleEvaluator eval = new DoubleEvaluator();
		
		return new Interval(eval.evaluate(aField.getText()), eval.evaluate(bField.getText()));
	}
	
}
