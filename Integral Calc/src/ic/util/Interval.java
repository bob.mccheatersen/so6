package ic.util;

public class Interval {
	
	public double a, b;
	
	public Interval(double a, double b) {
		this.a = a;
		this.b = b;
	}

	public double length() {
		return b - a;
	}
	
	public double map(Interval to, double num) {
		
		num -= a;
		num /= b - a;
		
		num *= to.b - to.a;
		num += to.a;
		
		return num;
	}
	
	public boolean inside(double d) {
		return a < d && d < b;
	}
	
	
}
