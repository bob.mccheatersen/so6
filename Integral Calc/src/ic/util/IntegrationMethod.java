package ic.util;

import com.fathzer.soft.javaluator.DoubleEvaluator;
import com.fathzer.soft.javaluator.StaticVariableSet;

public abstract class IntegrationMethod {

	public static final java.util.Vector<IntegrationMethod> LIST = new java.util.Vector<>();
	
	public static IntegrationMethod LEFT_SUM = new IntegrationMethod() {
		@Override
		public double integral(Function f, Interval interval, int subIntervals) {

			double subIntervalLength = interval.length()/subIntervals;

			double result = 0;

			DoubleEvaluator eval = new DoubleEvaluator();
			final StaticVariableSet<Double> vars = new StaticVariableSet<>();

			double errorMargin = subIntervalLength * 0.5;

			for(double d = interval.a;
					d < interval.b - errorMargin;
					d += subIntervalLength) {

				vars.set("x", d);
				result += eval.evaluate(f.getExpression(), vars);
			}

			result *= subIntervalLength;

			return result;
		}
		
		@Override
		public String toString() {
			return "Rectangle sum (left)";
		}
	};

	public static IntegrationMethod RIGHT_SUM = new IntegrationMethod() {
		@Override
		public double integral(Function f, Interval interval, int subIntervals) {

			double subIntervalLength = interval.length()/subIntervals;
			
			return LEFT_SUM.integral(f, new Interval(interval.a + subIntervalLength, interval.b + subIntervalLength), subIntervals);
		}
		
		@Override
		public String toString() {
			return "Rectangle sum (right)";
		}
	};

	public static IntegrationMethod TRAPEZOID_SUM = new IntegrationMethod() {

		@Override
		public double integral(Function f, Interval interval, int subIntervals) {

			double subIntervalLength = interval.length()/subIntervals;

			double result = 0;

			DoubleEvaluator eval = new DoubleEvaluator();
			final StaticVariableSet<Double> vars = new StaticVariableSet<>();

			double errorMargin = subIntervalLength * 0.5;

			for(double  d = interval.a + subIntervalLength;
					d < interval.b - errorMargin;
					d += subIntervalLength) {

				vars.set("x", d);
				result += eval.evaluate(f.getExpression(), vars);
			}

			vars.set("x", interval.a);
			double startAndEnd = eval.evaluate(f.getExpression(), vars);
			vars.set("x", interval.b);
			startAndEnd += eval.evaluate(f.getExpression(), vars);
			startAndEnd *= 0.5;

			result += startAndEnd;
			result *= subIntervalLength;
			
			return result;
		}
		
		@Override
		public String toString() {
			return "Trapezoid sum";
		}
		
	};
	
	private IntegrationMethod() {
		LIST.add(this);
	}

	public abstract double integral(Function f, Interval interval, int subIntervals);

}
