package ic.util;

import java.awt.Color;

import javax.swing.JOptionPane;

import com.fathzer.soft.javaluator.DoubleEvaluator;
import com.fathzer.soft.javaluator.StaticVariableSet;

import ic.gui.Frame;

public class Function {

	public static final String REGEX = "[a-zA-Z0-9]+\\(x\\)\\s*=.*";
	
	private String name;
	private String expression;
	private Color c;

	public Function(String name, String expression, Color c) {
		this.name = name;
		this.expression = expression;
		this.c = c;

		DoubleEvaluator eval = new DoubleEvaluator();
		final StaticVariableSet<Double> vars = new StaticVariableSet<>();
		vars.set("x", 1d);
		eval.evaluate(expression, vars);

	}

	public String getName() {
		return name;
	}

	public String getExpression() {
		return expression;
	}

	public static void addFunction(String definition) {
		try {

			String[] nameAndExpr = definition.split("=");
			
			String name = nameAndExpr[0].trim().replaceFirst("\\(x\\)", "");
			String expr = nameAndExpr[1].trim();
			
			IntegralMath.add(new Function(name, expr, new Color(255, 0, 0)));
			
		}catch(IllegalArgumentException e1){
			JOptionPane.showMessageDialog(Frame.getFrame().getInputPanel(),	"Failed to evaluate " + definition + " as a function.", "Input failed", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public Color getColor() {
		return c;
	}
	
	@Override
	public String toString() {
		return name + "(x) = " + expression;
	}

}
