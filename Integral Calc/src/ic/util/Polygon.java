package ic.util;

import java.awt.Color;

import com.fathzer.soft.javaluator.DoubleEvaluator;
import com.fathzer.soft.javaluator.StaticVariableSet;

public class Polygon {
	
	private Vector[] vertices;
	private Color c;
	
	public Polygon(Color c, Vector... vertices) {
		this.vertices = vertices;
		this.c = c;
		
	}
	
	public Color getColor() {
		return c;
	}
	
	public Vector[] getVertices() {
		return vertices;
	}
	
	public static void drawPolygon(Function f, Interval interval, int subIntervals, IntegrationMethod im) {
		
		double subIntervalLength = interval.length()/subIntervals;
		
		Vector[] vertices = null;
		
		if(im == IntegrationMethod.TRAPEZOID_SUM) {
			vertices = new Vector[subIntervals + 3];
			
			DoubleEvaluator eval = new DoubleEvaluator();
			final StaticVariableSet<Double> vars = new StaticVariableSet<>();
			
			double x, y;
			
			for(int i = 0; i < subIntervals; i++) {
				x = i * subIntervalLength + interval.a;
				vars.set("x", x);
				y = eval.evaluate(f.getExpression(), vars);
				vertices[i] = new Vector(x, y);
			}
			
			vars.set("x", interval.b);
			
			vertices[subIntervals + 0] = new Vector(interval.b, eval.evaluate(f.getExpression(), vars));
			vertices[subIntervals + 1] = new Vector(interval.b, 0);
			vertices[subIntervals + 2] = new Vector(interval.a, 0);
		}else if(im == IntegrationMethod.LEFT_SUM) {
			
			vertices = new Vector[subIntervals * 2 + 2];
			
			DoubleEvaluator eval = new DoubleEvaluator();
			final StaticVariableSet<Double> vars = new StaticVariableSet<>();		
			
			double x, y;
			
			for(int i = 0; i < subIntervals; i++) {
				x = i * subIntervalLength + interval.a;
				vars.set("x", x);
				y = eval.evaluate(f.getExpression(), vars);
				
				vertices[i * 2] = new Vector(x, y);
				vertices[i * 2 + 1] = new Vector(x + subIntervalLength, y);
			}
			
			vertices[subIntervals * 2 + 0] = new Vector(interval.b, 0);
			vertices[subIntervals * 2 + 1] = new Vector(interval.a, 0);
			
		}else if(im == IntegrationMethod.RIGHT_SUM) {
			
			vertices = new Vector[subIntervals * 2 + 2];
			
			DoubleEvaluator eval = new DoubleEvaluator();
			final StaticVariableSet<Double> vars = new StaticVariableSet<>();		
			
			double x, y;
			
			for(int i = 0; i < subIntervals; i++) {
				x = (i + 1) * subIntervalLength + interval.a;
				vars.set("x", x);
				y = eval.evaluate(f.getExpression(), vars);
				
				vertices[i * 2] = new Vector(x - subIntervalLength, y);
				vertices[i * 2 + 1] = new Vector(x, y);
			}
			
			vertices[subIntervals * 2 + 0] = new Vector(interval.b, 0);
			vertices[subIntervals * 2 + 1] = new Vector(interval.a, 0);
			
		}
		
		if(vertices != null) {
			IntegralMath.add(new Polygon(f.getColor(), vertices));
		}
		
	}
	
	public int getVertexCount() {
		return vertices.length;
	}

	@Override
	public String toString() {
		String str = "";
		
		for(Vector v : vertices) {
			str += v + ", ";
		}
		
		return str;
	}
	
	
	
}
