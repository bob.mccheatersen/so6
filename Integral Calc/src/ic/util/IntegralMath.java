package ic.util;

import java.awt.Color;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import ic.gui.Frame;

public class IntegralMath {
	
	public static final java.util.Vector<Function> FUNCTIONS = new java.util.Vector<>();
	static {
		FUNCTIONS.add(new Function("f", "x^5/100 + x^3/3 - 2*x + 6", new Color(255, 0, 0)));
	}
	public static Polygon currentPolygon;
	
	public static void add(Function f) {
		
		for(int i = 0; i < IntegralMath.FUNCTIONS.size(); i++) {
			if(IntegralMath.FUNCTIONS.get(i).getName().equals(f.getName())) {
				IntegralMath.FUNCTIONS.remove(i);
			}
		}
		
		FUNCTIONS.add(f);
		Frame.getFrame().getInputPanel().updateFunctionModel();
		Frame.getFrame().getGraphPanel().redraw();
		
	}
	
	public static void add(Polygon p) {
		currentPolygon = p;
		Frame.getFrame().getGraphPanel().redraw();
	}
	
	public static void main(String[] args) throws IOException {
		
		try(
				PrintWriter out = new PrintWriter(new FileWriter(new File("E:\\SO6\\RightSumError.txt")));
				){
			
			final Function testFunction = new Function("f", "x^5/100 + x^3/3 - 2*x + 6", new Color(255, 0, 0));
			
			final double trueIntegral = 4.777643200;
			final double a = 0.2;
			final double b = 1.2;
			final Interval interval = new Interval(a, b);
			final double intervalLength = interval.length();
			
			out.println("Right sum convergence");
			out.println("# of Intervals\tRight Sum Error");
			
			for(int i = 1; i <= 100; i++) {
				out.println(i + "\t" + Math.abs(trueIntegral - IntegrationMethod.RIGHT_SUM.integral(testFunction, interval, i)));
			}
			
		}
		

		
	}
	
}
